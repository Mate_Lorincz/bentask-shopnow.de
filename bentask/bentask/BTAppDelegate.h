//
//  BTAppDelegate.h
//  bentask
//
//  Created by Mate Lorincz on 08/06/14.
//  Copyright (c) 2014 ___FULLUSERNAME___. All rights reserved.
//

@interface BTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
