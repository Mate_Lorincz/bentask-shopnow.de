//
//  BTCollectionView.h
//  bentask
//
//  Created by Mate Lorincz on 08/06/14.
//  Copyright (c) 2014 Mate Lorincz. All rights reserved.
//

#import "BTCollectionViewProtocol.h"

@class BTViewController;
@class BTCollectionViewModel;

/**
 The custom collection view.
 */
@interface BTCollectionView : UIScrollView <UIScrollViewDelegate>

/**
 Designated initializer.
 @param frame The frame of collection view.
 @param collectionViewDelegate The delegate object.
 */
- (instancetype)initWithFrame:(CGRect)frame collectionViewDelegate:(id<BTCollectionViewDelegate>)collectionViewDelegate;

/** The delegate object that receives events happening with collection view. */
@property (weak, nonatomic) id<BTCollectionViewDelegate> collectionViewDelegate;

/** An array that contains the minimum number of cells needed for collection view. */
@property (strong, nonatomic, readonly) NSArray* staticCells;

@end
