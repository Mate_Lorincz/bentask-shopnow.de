//
//  BTCollectionView.m
//  bentask
//
//  Created by Mate Lorincz on 08/06/14.
//  Copyright (c) 2014 Mate Lorincz. All rights reserved.
//


#import "BTCollectionView.h"
#import "BTCollectionViewCell.h"
#import "BTViewController.h"
#import "BTCollectionViewModel.h"

@interface BTCollectionView () {
    NSArray *_contentArray;
    NSMutableArray *_mutableStaticCells;
    BTViewController <UIScrollViewDelegate> *_delegate;
    BTCollectionViewModel *_viewModel;
}

@end


@implementation BTCollectionView

- (instancetype)initWithFrame:(CGRect)frame collectionViewDelegate:(id<BTCollectionViewDelegate>)collectionViewDelegate {
    self = [super initWithFrame:frame];

    if (self) {
        self.delegate = self;
        _collectionViewDelegate = collectionViewDelegate;
        _viewModel = [_collectionViewDelegate dataModel];
        _contentArray = _viewModel.content;
        self.backgroundColor = [UIColor darkGrayColor];
        [self loadCells];
    }
    
    return self;
}

- (void)loadCells {
    NSUInteger numberOfStaticCellsNeeded = [_viewModel numberOfCellsNeededInView:self];
    _mutableStaticCells = [[NSMutableArray alloc] initWithCapacity:numberOfStaticCellsNeeded];

    if (numberOfStaticCellsNeeded) {
        self.contentSize = CGSizeMake(320, _contentArray.count * (kCellHeight + kCellSpacer) - kCellSpacer);

        for (int i = 0; i < numberOfStaticCellsNeeded; i++) {
            int cellOriginY = i * kCellHeight + i * kCellSpacer;
            BTCollectionViewCell *cell = [[BTCollectionViewCell alloc] initWithFrame:CGRectMake(0, cellOriginY, 320, kCellHeight)];
            cell.tag = kCellBaseTag + i;
            [self loadContentsForCell:cell atIndex:i];
            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture:)];
            [cell addGestureRecognizer:tapGesture];
            [self addSubview:cell];
            cell.displayed = [self cellIsDisplayed:cell];
            [_mutableStaticCells addObject:cell];
        }
    }
}

- (void)loadContentsForCell:(BTCollectionViewCell *)cell atIndex:(NSUInteger)index {
    
    if ([_contentArray[index] isKindOfClass:[UIImage class]]) {
        cell.type = BTCellTypeImage;
        cell.imageCellImageView.image = _contentArray[index];
        cell.imageCellLabel.text = [NSString stringWithFormat:@"Cell #%ld (BTImageCellType)", cell.tag - kCellBaseTag];
        cell.imageCellLabel.textAlignment = NSTextAlignmentCenter;
        cell.imageCellLabel.numberOfLines = 0;
        cell.imageCellLabel.adjustsFontSizeToFitWidth = YES;
    }
    else if ([_contentArray[index] isKindOfClass:[NSString class]]){
        cell.type = BTCellTypeText;
        cell.textCellLabel.text = [NSString stringWithFormat:@"Cell #%ld (BTTextCellType) - %@", cell.tag - kCellBaseTag, _contentArray[index]];
        cell.textCellLabel.numberOfLines = 0;
        cell.textCellLabel.adjustsFontSizeToFitWidth = YES;
    }
}

- (BOOL)cellIsDisplayed:(BTCollectionViewCell *)cell {
    UIView *superView = ((BTViewController *)_collectionViewDelegate).view;
    CGPoint cellOriginInSuperview = [superView convertPoint:CGPointZero fromView:cell];

    if (0 <= cellOriginInSuperview.y + kCellHeight && cellOriginInSuperview.y <= self.frame.size.height ) {
        return YES;
    }
    
    return NO;
}

- (void)displayCellIfNeeded {
    [self adjustDisplayedCells];
    
    if([self shouldLoadNextCell]) {
        BTCollectionViewCell *lastCell = _mutableStaticCells.lastObject;
        BTCollectionViewCell *newCell = _mutableStaticCells.firstObject;
        newCell.frame = CGRectMake(0, lastCell.frame.origin.y + kCellHeight + kCellSpacer, 320, kCellHeight);
        newCell.tag = lastCell.tag + 1;
        [self loadContentsForCell:newCell atIndex:newCell.tag - kCellBaseTag];
        
        for (NSUInteger i = 0; i < _mutableStaticCells.count - 1; i++) {
            _mutableStaticCells[i] = _mutableStaticCells[i+1];
        }
        
        _mutableStaticCells[_mutableStaticCells.count-1] = newCell;
    }

    if([self shouldLoadPreviousCell]) {
        BTCollectionViewCell *firstCell = _mutableStaticCells.firstObject;
        BTCollectionViewCell* newCell = _mutableStaticCells.lastObject;
        newCell.frame = CGRectMake(0, firstCell.frame.origin.y - kCellHeight - kCellSpacer, 320, kCellHeight);
        newCell.tag = firstCell.tag - 1;
        [self loadContentsForCell:newCell atIndex:newCell.tag - kCellBaseTag];

        for (NSUInteger i = _mutableStaticCells.count - 1; i >= 1; i--) {
            _mutableStaticCells[i] = _mutableStaticCells[i-1];
        }

        _mutableStaticCells[0] = newCell;
    }
}

- (void)adjustDisplayedCells {
    
    for (BTCollectionViewCell* cell in _mutableStaticCells) {
        
        if (cell.displayed != [self cellIsDisplayed:cell]) {
            
            if (cell.displayed) {
                
                if (_collectionViewDelegate && [_collectionViewDelegate respondsToSelector:@selector(collectionView:willHideCellAtIndex:)]) {
                    [_collectionViewDelegate collectionView:self willHideCellAtIndex:cell.tag - kCellBaseTag];
                }
                
                cell.displayed = NO;
                cell.hidden = YES;
                
                if (_collectionViewDelegate && [_collectionViewDelegate respondsToSelector:@selector(collectionView:didHideCellAtIndex:)]) {
                    [_collectionViewDelegate collectionView:self didHideCellAtIndex:cell.tag - kCellBaseTag];
                }
            }
            else {
                
                if (_collectionViewDelegate && [_collectionViewDelegate respondsToSelector:@selector(collectionView:willShowCellAtIndex:)]) {
                    [_collectionViewDelegate collectionView:self willShowCellAtIndex:cell.tag - kCellBaseTag];
                }
                
                cell.displayed = YES;
                cell.hidden = NO;
                
                if (_collectionViewDelegate && [_collectionViewDelegate respondsToSelector:@selector(collectionView:didShowCellAtIndex:)]) {
                    [_collectionViewDelegate collectionView:self didShowCellAtIndex:cell.tag - kCellBaseTag];
                }
            }
            
        }
    }
}

- (BOOL)shouldLoadNextCell {
    UIView *superView = ((BTViewController *)_collectionViewDelegate).view;
    BTCollectionViewCell *lastCell = _mutableStaticCells.lastObject;
    CGPoint lastCellOriginInSuperview = [superView convertPoint:CGPointZero fromView:_mutableStaticCells.lastObject];
    
    // Check last cell is fully visible in content
    if (lastCellOriginInSuperview.y + kCellHeight <= self.frame.size.height) {
        // Check has next item in content array
        if ((lastCell.tag - kCellBaseTag + 1 < _contentArray.count)) {
            return YES;
        }
    }
    
    return NO;
}

- (BOOL)shouldLoadPreviousCell {
    UIView *superView = ((BTViewController *)_collectionViewDelegate).view;
    BTCollectionViewCell *firstCell = _mutableStaticCells.firstObject;
    CGPoint firstCellOriginInSuperview = [superView convertPoint:CGPointZero fromView:_mutableStaticCells.firstObject];

    // Check first cell is fully visible in content
    if (firstCellOriginInSuperview.y >= 0) {
        // Check has previous item in content array
        if ((firstCell.tag - kCellBaseTag > 0)) {
            return YES;
        }
    }

    return NO;
}


#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self displayCellIfNeeded];
}


#pragma mark - UIGestureRecognizerDelegate

- (void)tapGesture:(UITapGestureRecognizer *)gestureRecognizer {
    
    if (_collectionViewDelegate && [_collectionViewDelegate respondsToSelector:@selector(collectionView:didSelectCell:)]) {
        [_collectionViewDelegate collectionView:self didSelectCell:(BTCollectionViewCell *)gestureRecognizer.view];
    }
}

- (NSArray *)staticCells {
    return _mutableStaticCells;
}

@end
