//
//  BTCollectionViewCell.h
//  bentask
//
//  Created by Mate Lorincz on 08/06/14.
//  Copyright (c) 2014 Mate Lorincz. All rights reserved.
//

typedef NS_ENUM(NSInteger , BTCellType) {
    BTCellTypeImage = 0,
    BTCellTypeText = 1
};

/**
 Cells for the custom collection view.
 */
@interface BTCollectionViewCell : UIView

/**
 Designated initializer.
 @param type The cell type.
 */
- (instancetype)initWithFrame:(CGRect)frame type:(BTCellType)type;

/** Label that displayed if cell type is BTCellTypeText. */
@property (strong, nonatomic) UILabel *textCellLabel;

/** Image view that displayed if cell type is BTCellTypeImage. */
@property (strong, nonatomic) UIImageView *imageCellImageView;

/** Label that displayed if cell type is BTCellTypeImage. */
@property (strong, nonatomic) UILabel *imageCellLabel;

/** The cell is displayed and fully- or partly visible in collection view */
@property (nonatomic) BOOL displayed;

/** Type of cell */
@property (nonatomic) BTCellType type;

@end
