//
//  BTCollectionViewCell.m
//  bentask
//
//  Created by Mate Lorincz on 08/06/14.
//  Copyright (c) 2014 Mate Lorincz. All rights reserved.
//

#import "BTCollectionViewCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation BTCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame type:(BTCellType)type {
    self = [super initWithFrame:frame];
    
    if (self) {
        _type = type;
        _displayed = NO;
        [self loadSubviewsForType:_type];
    }
    
    return self;
}

- (void)drawRect:(CGRect)rect {
    self.layer.cornerRadius = 15;
    self.layer.masksToBounds = YES;
}

- (void)setType:(BTCellType)type {
    _type = type;
    [self loadSubviewsForType:type];
}

- (void)loadSubviewsForType:(BTCellType)type {
    
    if (_type == BTCellTypeImage) {
        [self loadImageCellSubviews];
    }
    else {
        [self loadTextCellSubviews];
    }
}

- (void)loadImageCellSubviews {
    self.backgroundColor = [UIColor colorWithRed:216.0 / 255.0 green:13.0 / 255.0 blue:0.0 / 255.0 alpha:1.0];

    if (!_imageCellImageView) {
        _imageCellImageView = [[UIImageView alloc] initWithFrame:CGRectMake(kCellContentInset, kCellContentInset, kCellHeight - 2 * kCellContentInset,  kCellHeight - 2 * kCellContentInset)];
        [self addSubview:_imageCellImageView];
    }

    if (!_imageCellLabel) {
        _imageCellLabel = [[UILabel alloc] initWithFrame:CGRectMake(kCellHeight + kCellContentInset, kCellContentInset, 320 - kCellHeight - 3 * kCellContentInset, kCellHeight - 2 * kCellContentInset)];
        [self addSubview:_imageCellLabel];
    }
    
    [self hideSubviewsForType:_type];
}

- (void)loadTextCellSubviews {
    self.backgroundColor = [UIColor colorWithRed:253.0 / 255.0 green:208.0 / 255.0 blue:0.0 / 255.0 alpha:1.0];

    if (!_textCellLabel) {
        _textCellLabel = [[UILabel alloc] initWithFrame:CGRectMake(kCellContentInset, kCellContentInset, 320 - 2 * kCellContentInset, kCellHeight - 2 * kCellContentInset)];
        [self addSubview:_textCellLabel];
   }
    
    [self hideSubviewsForType:_type];
}

- (void)hideSubviewsForType:(BTCellType)type {
    _textCellLabel.hidden = !type;
    _imageCellImageView.hidden = type;
    _imageCellLabel.hidden = type;
}

@end
