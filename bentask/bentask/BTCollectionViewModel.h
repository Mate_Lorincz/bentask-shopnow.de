//
//  BTCollectionViewModel.h
//  bentask
//
//  Created by Mate Lorincz on 08/06/14.
//  Copyright (c) 2014 Mate Lorincz. All rights reserved.
//

/**
 Model for the custom collection view.
 */
@interface BTCollectionViewModel : NSObject

/** An array that contains items for cells. */
@property (weak, nonatomic, readonly) NSArray *content;

/**
 Designated initializer.
 @param content An array that contains items for cells.
 */
- (instancetype)initWithContent:(NSArray *)content;

/**
 Calculates the minimum number of cells that should be enough to reuse properly.
 @param view The custom collection view that takes the cells.
 @return NSUIneger Te minimum number of cells needed.
 */
- (NSUInteger)numberOfCellsNeededInView:(UIView *)view;

@end
