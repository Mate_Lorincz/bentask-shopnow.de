//
//  BTCollectionViewModel.m
//  bentask
//
//  Created by Mate Lorincz on 08/06/14.
//  Copyright (c) 2014 Mate Lorincz. All rights reserved.
//

#import "BTCollectionViewModel.h"
#import "BTCollectionViewCell.h"

@implementation BTCollectionViewModel {
    NSMutableArray *_contentArray;
}

- (instancetype)initWithContent:(NSArray *)content {
    self = [super init];
    
    if (self) {
        _contentArray = [content mutableCopy];
    }
    
    return self;
}

- (NSArray *)content {
    return _contentArray;
}

- (NSUInteger)numberOfCellsNeededInView:(UIView *)view {
    NSUInteger maxNumberOfCellsInContent = (NSUInteger)((view.frame.size.height / kCellHeight) + 2);
    return ( maxNumberOfCellsInContent > _contentArray.count) ? _contentArray.count : maxNumberOfCellsInContent;
}

@end