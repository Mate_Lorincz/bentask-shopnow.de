//
//  BTCollectionViewProtocol.h
//  bentask
//
//  Created by Mate Lorincz on 09/06/14.
//  Copyright (c) 2014 Mate Lorincz. All rights reserved.
//

@class BTCollectionView;
@class BTCollectionViewCell;
@class BTCollectionViewModel;

@protocol BTCollectionViewDelegate <NSObject>

- (BTCollectionViewModel *)dataModel;

@optional

- (void)collectionView:(BTCollectionView *)collectionView didSelectCell:(BTCollectionViewCell *)cell;
- (void)collectionView:(BTCollectionView *)collectionView willShowCellAtIndex:(NSInteger)index;
- (void)collectionView:(BTCollectionView *)collectionView didShowCellAtIndex:(NSInteger)index;
- (void)collectionView:(BTCollectionView *)collectionView willHideCellAtIndex:(NSInteger)index;
- (void)collectionView:(BTCollectionView *)collectionView didHideCellAtIndex:(NSInteger)index;

@end
