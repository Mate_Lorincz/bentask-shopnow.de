//
//  BTConstants.h
//  bentask
//
//  Created by Mate Lorincz on 08/06/14.
//  Copyright (c) 2014 Mate Lorincz. All rights reserved.
//

#ifndef bentask_BTConstants_h
#define bentask_BTConstants_h

/** Height of cells in collection view */
static int const kCellHeight = 60;

/** Space between cells in collection view */
static int const kCellSpacer = 5;

/** Inset of subviews in cell */
static int const kCellContentInset = kCellHeight / 10;

/** A unique base number for cell tags */
static int const kCellBaseTag = 1000;

/** Number of items in collection view */
static int const kHardcodedItemsCount = 200;

/** Hardcoded text for type of text cells */
static NSString * const kHardcodedTextItem = @"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

#endif
