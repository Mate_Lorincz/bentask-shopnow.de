//
//  BTViewController.h
//  bentask
//
//  Created by Mate Lorincz on 08/06/14.
//  Copyright (c) 2014 Mate Lorincz. All rights reserved.
//

#import "BTCollectionViewProtocol.h"

@class BTCollectionView;
@class BTCollectionViewModel;

/**
 A sample view controller that manages the custom collection view.
 */
@interface BTViewController : UIViewController <BTCollectionViewDelegate>

/** The view model */
@property (strong, nonatomic) BTCollectionViewModel *viewModel;

/**
 Returns the view model for collection view.
 @return BTCollectionViewModel The view model.
 */
- (BTCollectionViewModel *)dataModel;

@end
