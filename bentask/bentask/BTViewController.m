//
//  BTViewController.m
//  bentask
//
//  Created by Mate Lorincz on 08/06/14.
//  Copyright (c) 2014 Mate Lorincz. All rights reserved.
//

#import "BTViewController.h"
#import "BTCollectionViewModel.h"
#import "BTCollectionView.h"
#import "BTCollectionViewCell.h"

@interface BTViewController () {
    BTCollectionView *_collectionView;
}

@end


@implementation BTViewController

- (instancetype)init {
    self = [super init];

    if (self) {
        
        self.title = @"Ben Task";
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (!_viewModel) {
        _viewModel = [[BTCollectionViewModel alloc] initWithContent:[self hardcodedContents]];
        [self loadCollectionView];
    }

    for (BTCollectionViewCell *cell in _collectionView.staticCells) {
        [self proceedAnimationOnCell:cell];
    }
}


#pragma mark - Helper Methods

- (void)loadCollectionView {
    _collectionView = [[BTCollectionView alloc] initWithFrame:self.view.bounds collectionViewDelegate:self];
    [self.view addSubview:_collectionView];
}

- (NSArray *)hardcodedContents {
    NSMutableArray *hardcodedContents = [[NSMutableArray alloc] initWithCapacity:kHardcodedItemsCount];
    
    for (int i = 0; i < kHardcodedItemsCount; i++) {
 
        if (i % 2 == 0) {
            [hardcodedContents addObject:[UIImage imageNamed:@"SampleImage"]];
        } else {
            [hardcodedContents addObject:kHardcodedTextItem];
        }
    }

    return hardcodedContents;
}


#pragma mark - BTCollectionViewDelegate

- (void)collectionView:(BTCollectionView *)collectionView didSelectCell:(BTCollectionViewCell *)cell {
    [UIView animateWithDuration:.3 animations:^{
        cell.alpha = .5;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:.3 animations:^{
            cell.alpha = 1.0;
        }];
    }];
}

- (void)collectionView:(BTCollectionView *)collectionView willShowCellAtIndex:(NSInteger)index {
    for (BTCollectionViewCell* cell in _collectionView.staticCells) {
        if (cell.tag - kCellBaseTag == index) {
            cell.alpha = .0;
            break;
        }
    }
}

- (void)collectionView:(BTCollectionView *)collectionView didShowCellAtIndex:(NSInteger)index {
    for (BTCollectionViewCell* cell in _collectionView.staticCells) {
        if (cell.tag - kCellBaseTag == index) {
            [self proceedAnimationOnCell:cell];
            break;
        }
    }
}

- (BTCollectionViewModel *)dataModel {
    return _viewModel;
}

- (void)proceedAnimationOnCell:(BTCollectionViewCell *)cell {
    //1. Setup the CATransform3D structure
    CATransform3D rotation = CATransform3DMakeRotation( (90.0 * M_PI) / 180.0, 0.0, 0.7, 0.4);
    rotation.m34 = 1.0 / -600.0;
    
    //2. Define the initial state (Before the animation)
    cell.layer.shadowColor = [[UIColor blackColor] CGColor];
    cell.layer.shadowOffset = CGSizeMake(10, 10);
    cell.alpha = 0;
    
    cell.layer.transform = rotation;
    cell.layer.anchorPoint = CGPointMake(0, 0.5);
    
    
    //3. Define the final state (After the animation) and commit the animation
    [UIView beginAnimations:@"rotation" context:NULL];
    [UIView setAnimationDuration:0.8];
    cell.layer.transform = CATransform3DIdentity;
    cell.alpha = 1;
    cell.layer.shadowOffset = CGSizeMake(0, 0);
    [UIView commitAnimations];

    if (cell.frame.origin.x != 0) {
        cell.frame = CGRectMake(0, cell.frame.origin.y, 320, kCellHeight);
    }

    return;
}

@end
